#!/usr/bin/env python
# coding: utf-8

# Import libraries here
import datetime
import os
# Supress tensorflow output messages
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
from tensorflow.keras.models import load_model
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import joblib
import sys
from pathlib import PurePath


# Set up all file paths here
# NOTE: Arguments that need to be provided to the script are:
#  1. Number of predictors for the neural network
#  2. Number of quantities predicted by the neural network
#  3. Location of saved neural network model
#  4. Location of saved x scaler data
#  5. Location of saved y scaler data
#  6. Directory Location of reference all_inputs.csv file
#  7. Directory Location at which the protocol modified all_inputs.csv file should be saved to.
#  8. Are targets user defined? Acceptable values: True/False or 1/0
#  9. If targets are user defined, location of target information file.

# OPTIONAL. INTERNAL TESTING ONLY!
#  10. location of the protocol inputs file (height-weight-age wise)
#  11. Iteration number of the protocol being tested

starttime        = datetime.datetime.now()

# Neural Network related file paths.
num_predictors   = int(sys.argv[1])
num_predicted    = int(sys.argv[2])
nn_path          = sys.argv[3]
scaler_x_path    = sys.argv[4]
scaler_y_path    = sys.argv[5]

# Paths for files modified by the neural network.
ref_inputs_dir   = PurePath(sys.argv[6])
mod_inputs_dir   = PurePath(sys.argv[7])

# Check flag for user defined input and input file location.
user_def_check   = int(sys.argv[8])
user_target_file = PurePath(sys.argv[9])

# OPTIONAL. INTERNAL TESTING ONLY!
input_path       = sys.argv[10]
iter_num         = int(sys.argv[11])

# # PROTOTYPING CODE-----------------------------------------------------------------------------------------------------------------------------------

# # Neural Network related file paths.
# num_predictors   = 19+1-6
# num_predicted    = 7
# nn_path          = PurePath('/scratch/akashg/DNN_training/500k')
# scaler_x_path    = PurePath('/scratch/akashg/DNN_training/500k/500k_x.gz')
# scaler_y_path    = PurePath('/scratch/akashg/DNN_training/500k/500k_y.gz')

# # Paths for files modified by the neural network.
# ref_inputs_dir   = PurePath('/home/akashg/gen_LPN_ANN_training/tests_proto')
# mod_inputs_dir   = PurePath('/home/akashg/gen_LPN_ANN_training/tests_proto')

# # Check flag for user defined input and input file location.
# user_def_check   = bool(0)
# # user_target_file = PurePath(sys.argv[9])

# # OPTIONAL. INTERNAL TESTING ONLY!
# input_path       = PurePath('/scratch/akashg/protocol_test/filtered_input.csv')
# iter_num         = 1

# # END PROTOTYPING CODE-------------------------------------------------------------------------------------------------------------------------------


# ## Set targets

# Create a dataframe to store all the necessary info.
index = range(1)

# Columns for resting (30 predictors)
columns = ['HR','Mean Aortic Pressure','Systolic Aortic Pressure','Diastolic_Pressure','scale_SVR',\
           'Max RV volume','Min RV Volume','Mean PA Pressure','Systolic PA pressure','scale_PVR',\
           'Systemic capacitor scale','Mean_RA_Pressure','Mean_LA_Pressure','skewness']#,'tas_percent','t1_percent','Vra0','Vla0','CcsaR','csaR',\
#            'dsaR','CcsaL','csaL','dsaL','SVR_ref','PVR_ref']

targets = pd.DataFrame(0,index=index,columns = columns, dtype=float)

targets.columns = [c.replace(' ', '_') for c in targets.columns] # Replace all the spaces in the column names with underscore for easier access

# Check to see if a modified all_inputs.csv file exists. If yes, delete to prevent conflicts in calculations
fname = "all_inputs" + str(iter_num) + ".csv"

if os.path.exists(mod_inputs_dir/fname):
    try:
        os.remove(mod_inputs_dir/fname)
    except:
        print("Error while deleting file ", mod_inputs_dir/fname)
else:
    print("NOTE: Existing modified all_inputs file not found. This is not an error, and can be ignored.")

# Store the input data in the targets DataFrame.

if(user_def_check): # Checking if user defined inputs were specified in the arguments
    user_targets = pd.read_excel(io=user_target_file, sheet_name=0, header=0)

    hr         = user_targets.iloc[0,0]
    hr_ref     = 80
    ts         = 0.5 - (0.2/60)*(120 - hr)*(60/hr)  # Systolic time ratio
    co_rest    = user_targets.iloc[0,1]
    sap_rest   = user_targets.iloc[0,2]
    dap_rest   = user_targets.iloc[0,3]
    lvedv_rest = user_targets.iloc[0,4]
    lvesv_rest = user_targets.iloc[0,5]
    rvedv_rest = user_targets.iloc[0,6]
    rvesv_rest = user_targets.iloc[0,7]

else: # If user defined targets are not present read from height/weight/age data table.
    
    protocol_inputs = pd.read_csv(filepath_or_buffer = input_path, delimiter=',', header='infer', index_col=None,dtype=np.float32)
    [age,height,weight,ex_level,abs_vo2_max]         = protocol_inputs.iloc[(iter_num - 1),:]

    # Calculate resting LPN outputs (hemodynamic targets) from Pradhan et al., 2021. DOI:10.1007/s13239-021-00582-3
    hr     = -32.25 -0.36*age + 0.62*height +0.13*weight + 9.27          # Heart Rate
    hr_ref = 80
    
    lvedv_rest = -134.73 - 0.42*age + 1.04*height + 1.25*weight          # Left ventricular end diastolic volume
    lvesv_rest = -91.21 - 0.21*age + 0.68* height + 0.36*weight          # Left ventriculae end systolic volume
    rvedv_rest = -269.62 - 1.03*age + 2.44*height + 0.63*weight          # Right ventricular end diastolic volume
    rvesv_rest = -179.77 - 0.47*age  + 1.5*height + 0.01*weight          # Right ventricular end systolic volume
    co_rest    = (rvedv_rest  - rvesv_rest)*(hr/1000)                    # corrected CO at rest from RV volumes
    sap_rest   = 25.45 + 0.09*age + 0.4*height + 0.24*weight + 5.57      # Systolic Aortic pressure
    dap_rest   = 29.23 + 0.18*age + 0.15*height + 0.18*weight + 1.02     # Diastolic Aortic Pressure 

# Default values for resting conditions
mPAP       = 14.0                                            # Mean Pulmonary artery pressure.
lap_rest   = 7.2                                             # Left Atrial Pressure
sPAP       = (mPAP - 2)/0.61                                 # Sys. Pulmonary Artery Pressure from Chemla et al.,2004. DOI:10.1378/chest.126.4.1313
rap        = 3                                               # Right Atrial Pressure
ts         = 0.5 - (0.2/60)*(120 - hr)*(60/hr)               # Sytolic Time Ratio.
map_rest   = ts*sap_rest + (1 - ts)*dap_rest                 # Mean Aortic Pressure at rest
svr_rest   = (map_rest - 3)/( co_rest*1000/60 )              # Systemic Vascular Resistance (SVR).
pvr_rest   = (mPAP - lap_rest )/( co_rest*1000/60)           # Pulmonary Vascular resistance (PVR)

# Reference SVR and PVR set with finalised,corrected LPN on 2023 Oct. 18 2023
svr_ref    = 1.013633260185947                               # SVR of reference LPN
pvr_ref    = 0.078674175524849                               # PVR of reference LPN
tvr_ref    = svr_ref + pvr_ref                               # Total Vascular Resistance (TVR)
tvr_rest   = svr_rest + pvr_rest                             # Subject/Target/Case specific TVR

svrsf_rest = svr_rest/svr_ref                                # SVR Scaling factor
pvrsf_rest = pvr_rest/pvr_ref                                # PVR Scaling factor
tvrsf_rest = tvr_rest/tvr_ref                                # TVR Scaling factor

# Assign all targets to the dataframe used later
targets = targets.assign(HR=hr/hr_ref)
targets = targets.assign(Mean_Aortic_Pressure= map_rest)
targets = targets.assign(Systolic_Aortic_Pressure= sap_rest)
targets = targets.assign(Diastolic_Pressure=dap_rest)
targets = targets.assign(scale_SVR=svrsf_rest)
targets = targets.assign(Max_LV_Volume=lvesv_rest + (rvedv_rest - rvesv_rest))
targets = targets.assign(Min_LV_Volume=lvesv_rest)
targets = targets.assign(Max_RV_volume=rvedv_rest)
targets = targets.assign(Min_RV_Volume=rvesv_rest)
targets = targets.assign(Mean_PA_Pressure=15.0)         
targets = targets.assign(Systolic_PA_pressure=18.0)
targets = targets.assign(PA_Pulse_pressure=8.0)
targets = targets.assign(scale_PVR=pvrsf_rest)
targets = targets.assign(Systemic_capacitor_scale=tvrsf_rest ** (-4/3))
targets = targets.assign(Mean_RA_Pressure=3) # Constant Values for resting
targets = targets.assign(Mean_LA_Pressure=7.1) # Constant Values for resting
targets = targets.assign(skewness = 1.21)

'''Declare variables (arbitrary values) for left-ventricular end systolic pressure and left ventricular ref volume. 
EmaxLV and EmaxRV are needed, so they are calculated after NN is invoked.'''
LVESP = 0
Vlv0  = -200.0
RVESP = 0
Vrv0  = -200.0
# targets.iloc[0,:]

# First load the allinputs.csv file
dpath = PurePath(ref_inputs_dir)
inputs = pd.read_csv(dpath/"all_inputs.csv")

#--------------------------------------------------------------------------------------------------------------------------------------------
# Global Tuning
#--------------------------------------------------------------------------------------------------------------------------------------------

# Set up all the indices here
SVR_indices     = np.array([166,169,171,177,178,185,186,190,191,192])
PVR_indices     = np.array([195,196])
SysCap_indices  = np.array([165,168,170,175,176,183,184,187,188,189])       # All systemic capacitances
large_art_res   = np.array([177, 178])                                      # Rthao and Rabao
large_art_cap   = np.array([175,176])                                       # Cthao,Cabao

# Scale the reference resistances by calculated resting SVR and PVR factors
inputs.iloc[ SVR_indices , 2 ]    = inputs.iloc[ SVR_indices ,   2 ] * targets.iloc[0 , 4]
inputs.iloc[ large_art_res , 2 ]  = inputs.iloc[ large_art_res , 2 ] * targets.iloc[0 , 4]
inputs.iloc[ PVR_indices , 2 ]    = inputs.iloc[ PVR_indices ,   2 ] * targets.iloc[0 , 9]

# Leg scaling for resting
inputs.iloc[ 182, 2 ]             = inputs.iloc[ 182, 2 ] * targets.iloc[0,4]

# Now we use allometric scaling for ALL the capacitors 
inputs.iloc[ SysCap_indices , 2 ] = inputs.iloc[ SysCap_indices , 2 ] * targets.iloc[0 , 10]
inputs.iloc[ large_art_cap , 2 ]  = inputs.iloc[ large_art_cap ,  2 ] * targets.iloc[0 , 10]

# Scale lung capacitors
inputs.iloc[ 193 , 2 ]            = inputs.iloc[ 193 , 2 ] * targets.iloc[0 , 10]
inputs.iloc[ 194 , 2 ]            = inputs.iloc[ 194 , 2 ] * targets.iloc[0 , 10]

# Write the new allinputs_mod.csv file
# inputs.to_csv(mod_inputs_dir/fname2,index=False,header=True,float_format = "%.12f")

#--------------------------------------------------------------------------------------------------------------------------------------------
# DNN Execution
#--------------------------------------------------------------------------------------------------------------------------------------------
# Load the neural network and scaler data here
model    = load_model(nn_path)
scaler_x = joblib.load(scaler_x_path)
scaler_y = joblib.load(scaler_y_path)

# Scale the NN inputs before supplying them to the NN
input_x = np.reshape(a=np.array(targets.iloc[0,0:num_predictors]),newshape=((1,-1)) )
xscaled = scaler_x.transform( input_x )

# Call the neural network to get the predictions
DNN_predict = model.predict(xscaled)

# Do an inverse transform of the NN output to get the actual scaling factors
yhat = scaler_y.inverse_transform(DNN_predict)

# Assign the scaling factors here
inputs.iloc[  94 , 2 ] = inputs.iloc[  94 , 2 ] * targets.loc[0,'HR']  # scale_hr

inputs.iloc[ 106 , 2 ] = inputs.iloc[ 106 , 2 ] *    yhat[0,0]         # scale_EmaxRV
inputs.iloc[ 107 , 2 ] = inputs.iloc[ 107 , 2 ] *    yhat[0,1]         # scale_EoffsetRV
inputs.iloc[ 108 , 2 ] = inputs.iloc[ 108 , 2 ] *    yhat[0,2]         # scale_EmaxLV
inputs.iloc[ 109 , 2 ] = inputs.iloc[ 109 , 2 ] *    yhat[0,3]         # scale_EoffsetlV
inputs.iloc[ 172 , 2 ] = inputs.iloc[ 172 , 2 ] *    yhat[0,4]         # scale_Cao
inputs.iloc[ 0 ,   2 ] = inputs.iloc[ 0 ,   2 ] *    yhat[0,5]         # scale_init
inputs.iloc[ 208 , 2 ] = inputs.iloc[ 208 , 2 ] *    yhat[0,6]         # scale_Raov


# Filename of modified all_inputs.csv file
fname2 = "all_inputs" + str(iter_num) + ".csv"

# # To run the rest of the the protocol independent of the DNN.
# # Load the debug data

# fname2 = "all_inputs" + str(iter_num) + "_1.csv"

# x = pd.read_csv(filepath_or_buffer = '/home/akashg/gen_LPN_ANN_training/Corrected_LPN_Raov/x_test1.gz', compression = 'infer')
# y = pd.read_csv(filepath_or_buffer = '/home/akashg/gen_LPN_ANN_training/Corrected_LPN_Raov/y_test1.gz', compression = 'infer')

# x = x.iloc[:,1:]
# y = y.iloc[:,1:]

# x_unscaled = pd.DataFrame(x)
# y_unscaled = pd.DataFrame(y)

# #Assign the scaling factors here
# inputs.iloc[  94 , 2 ] = inputs.iloc[  94 , 2 ] * targets.loc[0,'HR']                     # scale_hr

# inputs.iloc[ 106 , 2 ] = inputs.iloc[ 106 , 2 ] * y_unscaled.iloc[(iter_num-1),0]         # scale_EmaxRV
# inputs.iloc[ 107 , 2 ] = inputs.iloc[ 107 , 2 ] * y_unscaled.iloc[(iter_num-1),1]         # scale_EoffsetRV
# inputs.iloc[ 108 , 2 ] = inputs.iloc[ 108 , 2 ] * y_unscaled.iloc[(iter_num-1),2]         # scale_EmaxLV
# inputs.iloc[ 109 , 2 ] = inputs.iloc[ 109 , 2 ] * y_unscaled.iloc[(iter_num-1),3]         # scale_EoffsetlV
# inputs.iloc[ 172 , 2 ] = inputs.iloc[ 172 , 2 ] * y_unscaled.iloc[(iter_num-1),4]         # scale_Cao
# inputs.iloc[ 0 , 2 ]   = inputs.iloc[ 0 , 2 ]   * y_unscaled.iloc[(iter_num-1),5]         # scale_init
# inputs.iloc[ 208 , 2 ] = inputs.iloc[ 208 , 2 ] * y_unscaled.iloc[(iter_num-1),6]         # scale_Raov

# # For Vlv0 and and Vrv0 calculations.
# yhat      = np.zeros((1,7))
# yhat[0,2] = y_unscaled.iloc[(iter_num-1),2]
# yhat[0,0] = y_unscaled.iloc[(iter_num-1),0]

#--------------------------------------------------------------------------------------------------------------------------------------------
# Reference Volume Determination.
#--------------------------------------------------------------------------------------------------------------------------------------------

# # Calculate Vlv0 from MongeGarcia et al.,2019
# Vlv0_ref     = inputs.iloc[78 , 2]
# ts           = 0.5 - (0.2/60)*(120 - hr)*(60/hr)               # Sytolic Time Ratio.
# map_rest     = ts*sap_rest + (1 - ts)*dap_rest                 # Mean Aortic Pressure at rest


# SV           = targets.iloc[0,7] - targets.iloc[0,8]
# LVESP        = ( map_rest/0.8721 ) - (SV*0.051565)
# # Use the dummy objects values to calculate Vlv0 for the actual subject
# EmaxLV_dummy = inputs.iloc[ 108 , 2 ] * yhat[0,2] * inputs.iloc[ 75 , 2 ]
# Vlv0         = targets.iloc[0,6] - (LVESP/EmaxLV_dummy )
# Vlv0_ref     = inputs.iloc[ 111 , 2 ] * inputs.iloc[ 78 , 2 ]
# Vlv0_scale   = Vlv0/Vlv0_ref

# inputs.iloc[ 111 , 2 ] = inputs.iloc[ 111 , 2 ] * Vlv0_scale        # scale_Vlv0

# # Estimate RVESP as 2/3 of systolic + mean PA pressure
# Vrv0_ref     = inputs.iloc[78,2]
# mPAP_rest    = targets.iloc[0,8]
# sPAP_rest    = targets.iloc[0,9]

# EmaxRV_dummy = inputs.iloc[ 106 , 2 ] * yhat[0,0] * inputs.iloc[ 73 , 2 ]
# RVESP        = (mPAP_rest + sPAP)*(2/3)
# Vrv0         = (rvesv_rest - (RVESP/EmaxRV_dummy))
# Vrv0_ref     = inputs.iloc[ 77 , 2 ]
# Vrv0_scale   = Vrv0/Vrv0_ref

# inputs.iloc[ 110 , 2 ] = inputs.iloc[ 110 , 2 ] * Vrv0_scale        # scale_Vrv0

# Write the new allinputs_mod.csv file
inputs.to_csv(mod_inputs_dir/fname2,index=False,header=True,float_format = "%.12f")

# print(f"Vlv0: {Vlv0}")
# print(f"LVESP for Vlv0 calculation: {LVESP}")
# print(f"Vlv0 scaling factor for Vlv0 calculation: {Vlv0_scale}")
# print(f"EmaxLV for Vlv0 calculation: {EmaxLV_dummy}")

# print(f"Vrv0: {Vrv0}")
# print(f"RVESP for Vlv0 calculation: {RVESP}")
# print(f"EmaxRV for Vrv0 calculation: {EmaxRV_dummy}")

# Calculate time for execution
endtime = datetime.datetime.now()
time_elapsed = endtime - starttime
time_elapsed

print('Protocol executed succesfully.')
print(f'Elapsed protocol execution time: {time_elapsed!r} \n')