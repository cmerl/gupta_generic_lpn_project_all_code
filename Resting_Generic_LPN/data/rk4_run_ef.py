# py_rk4 is a pre-compiled library version of the rk4_in.f90 code that python can directly call.
# The pre-compiled library will have a long name like: py_rk4.cpython-37m-x86_64-linux-gnu.so
# This program needs the output folder to be specified through an input argument

import numpy as np
from scipy import signal
import pandas as ps
import os
import sys
# from os.path import dirname
# sys.path.append(dirname(__file__))
# sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import python_rk4

# results = np.zeros([100000,60],dtype = float)
a = np.zeros([100000,63],dtype = float)
# x_sum = np.zeros([3,63],dtype = float) # Store the x summary data (max,min,mean in that order)

# Pick up the output from the fortran rk4 code and store it in results
(a,lcycle) = python_rk4.rk4()
#print(lcycle)

# Trim the rows containing only zeros
# a = a[~np.all(a == 0, axis=0)]
a = np.delete(a,np.where(~a.any(axis=1))[0], axis=0)

# # Append a row and column of zeros so that the row and column indices start from 1, rather than 0
# # First get the new dimensions of a
# row,col = a.shape
# # Now create 1D arrays of the right size
# row_array = np.zeros([1,col],dtype = float)
# col_array = np.zeros([row+1,1],dtype = float)

# # Append these arrays to the front of array a
# a       = np.append(row_array,a, axis = 0)
# results = np.append(col_array,a, axis = 1)

# x   = [] # Temp variable store output of find_peaks function
# x_p = [] # Contains actual peak indices
# x_h = [] # Stores heights of the peaks
# combined  = []
# height_properties = {}

# for i in range(1,63):
#     if i==1:
#         # This just shifts the list index by one so that indices are consistent with the rk4 code
#         x_p.append([0]) 
#         x_h.append([0])
    
#     if i == 61:
#         # Calculate LVEF
#         x_sum[0,i] = np.amax(results[-lcycle:,25])
#         x_sum[1,i] = np.amin(results[-lcycle:,25])
        
#         if x_sum[0,i] == 0 :
#             print("NOTE:Division by zero occured while calculating LVEF in directory" + os.path.dirname(os.path.realpath(__file__)))
#             x_sum[2,i] = 0
#             continue
    
#         x_sum[2,i] = (x_sum[0,i] - x_sum[1,i])/x_sum[0,i]
#         continue
    
#     if i == 62:
#         # Calculate RVEF
#         x_sum[0,i] = np.amax(results[-lcycle:,21])
#         x_sum[1,i] = np.amin(results[-lcycle:,21])
        
#         if x_sum[0,i] == 0 :
#             print("NOTE:Division by zero occured while calculating RVEF in directory" + os.path.dirname(os.path.realpath(__file__)))
#             x_sum[2,i] = 0
#             continue
        
#         x_sum[2,i] = (x_sum[0,i] - x_sum[1,i])/x_sum[0,i]
#         continue
    

#     x_sum[0,i]           = np.amax(results[-lcycle:,i])
#     x_sum[1,i]           = np.amin(results[-lcycle:,i])
#     x_sum[2,i]           = np.sum(results[-lcycle:,i])/lcycle
#     heightmax            = 1.1*x_sum[0,i]
#     heightmin            = 0.9*x_sum[1,i]
#     x, height_properties = signal.find_peaks(results[-lcycle:,i], height=[heightmin,heightmax])
#     x_p.append(x)
#     x_h.append(height_properties["peak_heights"])

# # Create a combined list
# x_p.extend(x_h)

# fpath = str(sys.argv[1]) + "peak_data.txt"
# # Write out the data
# with open(fpath, 'w') as output:
#     for row in x_p:
#         output.write(str(row) + '\n')

fpath2 = str(sys.argv[1]) + "AllData.csv"
ps.DataFrame(a).to_csv(fpath2, header=None, index=None,float_format='%8.6f',line_terminator='\n')