"""
This code is for collating the generated output data from the LPN runs
Created on Tue Jan 04 2021
@author: Akash Gupta akashg@clemson.edu
"""

import os
import sys
import numpy as np
from pathlib import PurePath
import pandas as pd
import math

endnum = int(sys.argv[1])
count  = int(sys.argv[2])
tasks  = int(sys.argv[3])
nfiles = math.ceil(endnum/tasks)

start = 1
stop = 1

if (count == 1):
    start = 1
    stop  = count*nfiles
elif (count < tasks):
    start = (count-1)*nfiles + 1
    stop  = count*nfiles
else:
    stop  = endnum
    start = (count-1)*nfiles + 1

# print(f'count = {count}')

top_dir = PurePath(os.path.dirname(os.path.realpath(__file__)))
oname   = "prelim" + sys.argv[4] + "_" + str(count) + ".csv"
opath   = top_dir/"output"/oname
t       = pd.DataFrame()

for i in range(start, stop+1):
    try:
        fpath  = top_dir/sys.argv[5]/str(i)/"summary_data.csv"
        # Load the ANN_range file from the specified folder
        domain = np.genfromtxt(fname=fpath, delimiter=",", dtype=np.float64)

        # Transform the output data to a pandas dataframe
        out    = pd.DataFrame(domain)
        
        # Append the ouput data to existing dataframe t
        t      = pd.concat([t, out], axis=0, join='outer', copy=False, ignore_index=True)

        if (float(i) % 50000 == 0):
            print(f"Directory number {i} finished.")

    except IOError:
        # Create a NaN array if a file was not found and append to file
        domain      = np.zeros((3, 63))

        # Transform to a pandas dataframe
        out         = pd.DataFrame(domain)
        # Transpose the dataframe so that we write out the results row-wise
        t           = pd.concat([t, out], axis=0, join='outer', copy=False, ignore_index=True)
        print(f"Directory number {i} was empty, replaced data with 0s.")

try:
    t.drop(t.columns[[0]], axis=1, inplace=True)

except IndexError:
    print(
        f"A directory was empty between range {start} and {(stop+1)}, dataframe t has no columns to drop.")

# Write out to a csv file in append mode
t.to_csv(opath, mode='a', index=False, header=False, float_format='%.8f')