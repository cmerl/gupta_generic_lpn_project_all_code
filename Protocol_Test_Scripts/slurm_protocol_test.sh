#!/bin/bash

#--------------------------------------------------------------
#SBATCH --job-name prot_test
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
#SBATCH --cpus-per-task=1
#SBATCH --gpus-per-node v100:1
#SBATCH --mem=14gb
#SBATCH --time=06:00:00
#SBATCH --constraint interconnect_hdr
#--------------------------------------------------------------

# jobperf -record -w -http -rate 10s &
module purge
module load parallel/20220522
base_dir=$SLURM_SUBMIT_DIR
cd $base_dir

declare -i tasks
num1=1
num2=500
range_size=23
tasks=$SLURM_CPUS_PER_TASK*$SLURM_NTASKS_PER_NODE*$SLURM_NNODES
echo "tasks=$tasks"

# Do this to prevent output data overwrite
n=$(date +%d-%b-%Y-%T)
data_dir="data"
mkdir -p $base_dir/test_dir/$data_dir


rsync -av /scratch/akashg/Protocol_Test_Scripts/* /scratch/akashg/Protocol_Test_Scripts/test_dir/ --exclude 'mamba-root' --exclude 'oneapi' --exclude 'test_dir' \
--exclude 'ref_files' --exclude 'ref_all_inputs'

srun hostname > hostnames
time seq $num1 $num2|parallel --sshloginfile hostnames -j$SLURM_CPUS_PER_TASK "mkdir -p $base_dir/test_dir/$data_dir/{} ; cp -r $base_dir/ref_files/* $base_dir/test_dir/$data_dir/{}"

# Activate the mamba shell and tensorflow environment
source /home/akashg/mambaforge/bin/activate tf_2.16
conda info --envs

############################################################## Protocol invocation starts here ####################################################################

# NOTE: Arguments that need to be provided to the script are:
#  1. Number of predictors for the neural network
#  2. Number of quantities predicted by the neural network
#  3. Location of saved neural network model
#  4. Location of saved x scaler data
#  5. Location of saved y scaler data
#  6. Directory Location of reference all_inputs.csv file
#  7. Directory Location at which the protocol modified all_inputs.csv file should be saved to.
#  8. Are targets user defined? Acceptable values: True/False or 1/0
#  9. If targets are user defined, location of target information file.

# OPTIONAL. INTERNAL TESTING ONLY!
#  10. location of the protocol inputs file (height-weight-age wise)
#  11. Iteration number of the protocol being tested

# Set up the arguments here
num_predictors=14
num_predicted=7
nn_path="/scratch/akashg/DNN_training/32w_std_scaler_1.5mil/DNN.keras"
scaler_x_path="/scratch/akashg/DNN_training/32w_std_scaler_1.5mil/scaler_x.gz"
scaler_y_path="/scratch/akashg/DNN_training/32w_std_scaler_1.5mil/scaler_y.gz"
input_path="/scratch/akashg/Protocol_Test_Scripts/filtered_input.csv"
ref_inputs_dir="/scratch/akashg/Protocol_Test_Scripts/ref_all_inputs/"
user_def_flag=0

# export TF_ENABLE_ONEDNN_OPTS=1
# export KMP_AFFINITY=granularity=fine,compact,1,0
# export KMP_BLOCKTIME=0
# export TF_ENABLE_MKL_NATIVE_FORMAT=1

# Actually invoke the protocol, and DNN sequentially to do the tuning. Parallel execution had memory problems.
for i in `seq $num1 $num2`;
do
    python3 -W "ignore" $base_dir/test_dir/"restructure_protocol.py" $num_predictors \
    $num_predicted \
    $nn_path \
    $scaler_x_path \
    $scaler_y_path \
    $ref_inputs_dir \
    $base_dir/test_dir/$data_dir/$i/ \
    $user_def_flag \
    $user_def_flag \
    $input_path \
    $i
done

# Now, do the setup necessary to run the LPN, and then execute each LPN. Note the quatation marks at the end of line 107. they are necessary, and easy to miss.
time seq $num1 $num2|parallel --sshloginfile hostnames -j$SLURM_CPUS_PER_TASK "
# SSH setup
export LD_PRELOAD=/usr/lib64/libcrypto.so.1.1:$LD_PRELOAD
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/software/spackages/linux-centos8-x86_64/gcc-8.3.1/libffi-3.3-rzw344kkj4vjnvesiaeawhgfhamaut5t/lib64;

# Activate the mamba environment
source /home/akashg/mambaforge/bin/activate tnsrflw

############################################################## LPN setup starts here #############################################################################

# Run python script to create all the parameters.f and initvals.f files
cd $base_dir;\
python3 "$base_dir/test_dir/gen_input.py" $base_dir/test_dir/$data_dir/{}/;

# Intel compilers and libraries setup:
source /home/akashg/intel/oneapi/setvars.sh /home/akashg/intel/oneapi/>/dev/null 2>&1

# IMPORTANT NOTE: Move to the directory where the fortran file needs to be compiled. Without this, the compiled rk4 library will NOT be generated.
cd $base_dir/test_dir/$data_dir/{}

############################################################## LPN setup ends here #############################################################################

# Compile and run all instances of the LPN
python -m numpy.f2py -c -m python_rk4 $base_dir/test_dir/$data_dir/{}/py_rk4_in.f90 --compiler=intelem --fcompiler=intelem --f90flags='' --opt='-fast' --include-paths $base_dir/test_dir/$data_dir/{}>/dev/null 2>&1;
python3 $base_dir/test_dir/$data_dir/{}/rk4_run_ef.py $base_dir/test_dir/$data_dir/{}/

# Now remove the unnecessary files
# cd $base_dir/test_dir/$data_dir/{};
# shopt -s extglob;
# rm -- !(summary_data.csv);
"

mkdir -p "$base_dir/test_dir/output"

# Collate the output data
# Arguments are:
# 1. Number of combinations
# 2. Timestamp for ANN domain output file
# 3. Name of data directory
# time seq 1 "$tasks"|parallel --sshloginfile $PBS_NODEFILE -j$NCPUS "python3 $base_dir/test_dir/collate_parallel.py $num2 {} $tasks $n $data_dir"
time seq 1 $tasks|parallel --sshloginfile hostnames -j$SLURM_CPUS_PER_TASK "module load anaconda3/2023.09-0
export LD_PRELOAD=/usr/lib64/libcrypto.so.1.1:$LD_PRELOAD
# Activate the mamba shell
source /home/akashg/mambaforge/bin/activate tnsrflw
python3 $base_dir/test_dir/collate_parallel.py $num2 {} $tasks $n $data_dir"

# Compile all collated date into a single file.
time python3 $base_dir/test_dir/final_compile.py $base_dir/test_dir/output $n $tasks