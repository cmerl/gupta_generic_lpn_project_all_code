#!/bin/bash

#--------------------------------------------------------------
#SBATCH --job-name data_gen
#SBATCH --nodes 75
#SBATCH --tasks-per-node 1
#SBATCH --cpus-per-task 16
#SBATCH --mem 8gb
#SBATCH --time 10:00:00
#SBATCH --constraint chip_manufacturer_intel
#--------------------------------------------------------------

module purge
module load parallel/20220522
module list

cd $SLURM_SUBMIT_DIR


#Set up important variables here
declare -i nfiles
declare -i tasks
num1=1
num2=500000
tasks=$SLURM_CPUS_PER_TASK*$SLURM_NTASKS_PER_NODE*$SLURM_NNODES
echo "tasks=$tasks"

# Do this to prevent output data overwrite
n=$(date +%d_%b_%Y-%T)
data_dir="data"
mkdir -p $data_dir

# Activate the tf_smt environment with the smt library
source /home/akashg/mambaforge/bin/activate tnsrflw
conda info --envs

cd $SLURM_SUBMIT_DIR

# Perform LHS sampling and generate the range data file
# Arguments are:
# 1. Number of combinations
# 2. Timestamp for ANN range output file (To prevent files from being overwritten)
# 3. Name of the directory containing all the generated data
time python3 range_gen_no_log.py $num2 $n $data_dir

# Set access permissions for the range file
chmod 755 $SLURM_SUBMIT_DIR/"ANN_range$n.csv"

# Create a hostnames file so that gnu-parallel can connect to multiple nodes.
srun hostname > hostnames

time seq $num1 $num2|parallel --sshloginfile hostnames -j$SLURM_CPUS_PER_TASK "
# SSH setup
export LD_PRELOAD=/usr/lib64/libcrypto.so.1.1:$LD_PRELOAD

# Create directories specified in the sequence( num1 to num2) and copy reference 
# files from lpn folder into each directory
cd $SLURM_SUBMIT_DIR
mkdir -p $SLURM_SUBMIT_DIR/$data_dir/{} ; cp -r $SLURM_SUBMIT_DIR/lpn/* $SLURM_SUBMIT_DIR/$data_dir/{}

# Activate the mamba shell
source /home/akashg/mambaforge/bin/activate tnsrflw

# Create all of the allinputs.csv files
# Arguments are:
# 1. Data directory
# 2. Folder number
# 3. Timestamp for ANN range output file
# 4. Directory from where PBS file is submitted (i.e. also location of range data file)
python3 -W "ignore" "$SLURM_SUBMIT_DIR/create_inputs.py" $data_dir {} $n $SLURM_SUBMIT_DIR

# Run python script to create all the parameters.f and initvals.f files
cd $SLURM_SUBMIT_DIR
python3 -W "ignore" "$SLURM_SUBMIT_DIR/gen_input.py" $SLURM_SUBMIT_DIR/$data_dir/{}/

# Intel compilers and libraries setup:
source /home/akashg/intel/oneapi/setvars.sh /home/akashg/intel/oneapi/>/dev/null 2>&1

# IMPORTANT NOTE: Move to the directory where the fortran file needs to be compiled. Without this, the compiled rk4 library will NOT be generated.
cd $SLURM_SUBMIT_DIR/$data_dir/{}

export FC=ifort
export CC=icc

# Compile and run all instances of the LPN
python -m numpy.f2py -c -m python_rk4 $SLURM_SUBMIT_DIR/$data_dir/{}/py_rk4_in.f90 --f90flags='' --compiler=intelem --fcompiler=intelem --opt='-fast' --include-paths $SLURM_SUBMIT_DIR/$data_dir/{}>/dev/null 2>&1;
python3 -W "ignore" $SLURM_SUBMIT_DIR/$data_dir/{}/rk4_run_ef.py $SLURM_SUBMIT_DIR/$data_dir/{}/

# Now remove the unnecessary files
cd $SLURM_SUBMIT_DIR/$data_dir/{};
shopt -s extglob;
rm -- !(summary_data.csv);
"

mkdir -p "$SLURM_SUBMIT_DIR/output"

# Collate the output data
# Arguments are:
# 1. Number of combinations
# 2. Timestamp for ANN domain output file
# 3. Name of data directory
time seq 1 $tasks|parallel --sshloginfile hostnames -j$SLURM_CPUS_PER_TASK "
export LD_PRELOAD=/usr/lib64/libcrypto.so.1.1:$LD_PRELOAD

# Activate the mamba shell
source /home/akashg/mambaforge/bin/activate tnsrflw

python3 $SLURM_SUBMIT_DIR/collate_parallel.py $num2 {} $tasks $n $data_dir"

time python3 $SLURM_SUBMIT_DIR/final_compile.py $SLURM_SUBMIT_DIR/output $n $tasks