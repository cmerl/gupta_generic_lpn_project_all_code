#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon August 31 10:25:08 2020

@author: Akash Gupta akashg@clemson.edu
"""
import numpy as np
import pandas as pd
from smt.sampling_methods import LHS
from pathlib import PurePath
import os
import sys

# ----------------------------------------------------------------------------------------------------------------------------
""" Generate samples using LHS
    Note: All of the limits defined below are for the scaling factors for the refernce LPN parameters
    Serial numbers represent index numbers(starting from zero) from all_inputs.csv
    Serial Number + 2 is the actual row number in all_inputs.csv
"""
# --------------------------------------------------------------------------------------------
# # x Limits set
# xlimits = np.array([[0.7, 2.25],        # 0. scale_init
#                     [0.6, 1.30],        # 94. hr           Heart Parameters start here (Row 96)
#                     [0.5, 5.0],         # 106. EmaxRV
#                     [0.5, 7.5],        # 107. EoffsetRV
#                     [0.5, 6.0],         # 108. EmaxLV
#                     [0.5, 5.0],        # 109. EoffsetLV
#                     # [-2.0, 2.0],      # 110. Vrv0
#                     # [-5.0, 5.0],      # 111. Vlv0
#                     # [0.001, 2.0],     # 112. AV_couple    Heart Parameters end here(Row 114)
#                     # [0.001, 2.0],     # 120. Lao          Row 122
#                     [0.5, 10.0],        # 172. Cao          Row 174
#                     # [0.001, 2.0],     # 118. Lpul         Row 120
#                     # [0.0001, 5.0],    # 193. and 194. Clung            Row 195 and 196
#                     [0.75, 1.35],         # SVR scale #166,169,171,177,178,185,186,190-192  Rows 168,171,173,179,180,187,188,192-194 NOTE: Coronary and leg resistance is not included for exercise. For resting leg resistance is included!
#                     [0.15, 1.4],         # PVR scale #195,196 Rows 197,198
#                     # [0.0001, 2.0],    # Leg R scale #182, Row 184. NOTE: For resting, this is a part of SVR. Should be seperate for exercise!
#                     # [0.0001, 2.0],    # Leg C (Clega and Clegv) scale #180,181 Rows 182,183
#                     [1.0, 1.0],         # Systemic C scale (All C's except in the legs and coronaries) #165,168,170,175,176,183,184,187-189 Rows 167,170,172,177,178,185,186,189-191
#                     [0.1, 3.0],         # Scale Raov Row #210
#                     ])
# ----------------------------------------------------------------------------------------------------------------------------
# x Limits set
xlimits = np.array([[0.6, 1.4],        # 0. scale_init
                    [0.7, 1.30],        # 94. hr           Heart Parameters start here (Row 96)
                    # [0.1, 4.0],       #  95. tas_percent
                    # [0.1, 5.0],       #  96. t1_percent
                    # [0.0, 6.0],       #  97. Vra0
                    # [0.0, 6.0],       #  98. Vla0
                    # [0.1, 6.0],       #  99. CcsaR
                    # [0.1, 6.0],       # 100. csaR
                    # [0.1, 6.0],       # 101. dsaR
                    # [0.1, 6.0],       # 102. CcsaL
                    # [0.1, 6.0],       # 103. csaL
                    # [0.1, 2.0],       # 104. dsaR
                    # [0.1, 2.0],       # 105. dsaL
                    [0.9, 7.0],         # 106. EmaxRV
                    [0.5, 10.0],        # 107. EoffsetRV
                    [0.5, 7.0.0],         # 108. EmaxLV
                    [0.9, 10.0],        # 109. EoffsetLV
                    # [-2.0, 2.0],      # 110. Vrv0
                    # [-5.0, 5.0],      # 111. Vlv0
                    # [0.001, 2.0],     # 112. AV_couple    Heart Parameters end here(Row 114)
                    # [0.001, 2.0],     # 120. Lao          Row 122
                    [0.25, 5.0],        # 172. Cao          Row 174
                    # [0.001, 2.0],     # 118. Lpul         Row 120
                    # [0.0001, 5.0],    # 193. and 194. Clung            Row 195 and 196
                    [0.9, 1.75],         # SVR scale #166,169,171,177,178,185,186,190-192  Rows 168,171,173,179,180,187,188,192-194 NOTE: Coronary and leg resistance is not included for exercise. For resting leg resistance is included!
                    [0.15, 3.5],         # PVR scale #195,196 Rows 197,198
                    # [0.0001, 2.0],    # Leg R scale #182, Row 184. NOTE: For resting, this is a part of SVR. Should be seperate for exercise!
                    # [0.0001, 2.0],    # Leg C (Clega and Clegv) scale #180,181 Rows 182,183
                    [1.0, 1.0],         # Systemic C scale (All C's except in the legs and coronaries) #165,168,170,175,176,183,184,187-189 Rows 167,170,172,177,178,185,186,189-191
                    [0.25, 1.5],         # Scale Raov Row #210
                    ])
# Convert to log-scale
x_lim = pd.DataFrame(xlimits)
x_lim_log = np.log(x_lim)

# Set sampling method, limits and a random seed.
# sampling = LHS(xlimits=np.array(x_lim_log), random_state=701)
sampling = LHS(xlimits=np.array(x_lim_log), random_state=20240311)

# Set the number of samples to be generated, i.e, num will be the number of all_inputs.csv files generated
num         = int(sys.argv[1])
name        = sys.argv[2]
data_dir_in = sys.argv[3]

# Actual sampling happens here
x = sampling(num)

# Undo the conversion to the log-scale
x = np.exp(x)


# Convert to DataFrame for csv export and column operations
x_df = pd.DataFrame(x)

# Since the systemic C scale can be calculated, do so here and replace the appropriate column in x.
svr_ref = 1.00711244778888
pvr_ref = 0.044973261008228
tvr_ref = svr_ref + pvr_ref

tvr             = np.add( np.multiply(svr_ref,x_df.iloc[:,7])  , (np.multiply(pvr_ref,x_df.iloc[:,8]) ) )
tvr_scale       = np.divide(tvr, tvr_ref)
x_df.iloc[:,9]  = np.power(tvr_scale, (-4/3))

# ----------------------------------------------------------------------------------------------------------------------------
# Write out the sampled points to seperate AllInputs.csv files

# This is the top level directory which will have the refernce all_inputs file and lpn folder
top_dir  = PurePath(os.path.dirname(os.path.realpath(__file__)))
lpn_path = top_dir/"lpn"

# Create a data directory for all of the files
data_dir = top_dir/data_dir_in
name_str = "ANN_range" + name + ".csv"

# Read the reference all_inputs.csv file
inputs   = pd.read_csv(top_dir/"all_inputs.csv")

# Output all range values
x_df.columns = ['scale_init', 'scale_hr', 'scale_EmaxRV', 'scale_EoffsetRV', 'scale_EmaxLV', 'scale_EoffsetLV', 'scale_Cao',
                'scale_SVR', 'scale_PVR', 'scale_systemicC', 'scale_Raov']
x_df.to_csv(top_dir/name_str, index=False, header=True, float_format="%8.6f")