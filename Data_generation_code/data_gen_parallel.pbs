#PBS -N no_log_sampling
#PBS -l select=100:ncpus=16:chip_manufacturer=intel:mem=32gb:interconnect=fdr,walltime=14:00:00
#PBS -j oe

module load anaconda3/2022.10-gcc/9.5.0 
module load gnu-parallel/20220722
module list
# set -x

# Needed for ssh issues
export LD_PRELOAD=/usr/lib64/libcrypto.so.1.1:$LD_PRELOAD

cd $PBS_O_WORKDIR
base_dir=$PBS_O_WORKDIR

# Create a tempdir for each node
pbsdsh hostname

# cat $PBS_NODEFILE | uniq > nodes.txt
# count=`cat nodes.txt | wc -l`
# time cat $PBS_NODEFILE | parallel --sshloginfile nodes.txt -j0 "

cd $base_dir;
# cp /home/akashg/intel/oneapi.tgz $base_dir
# cp /home/akashg/mambaforge/Mambaforge-23.11.0-0-Linux-x86_64.sh $base_dir
# tar xzf oneapi.tgz
# bash ./Mambaforge-23.11.0-0-Linux-x86_64.sh -b -p "$base_dir/mamba-root"
# #>/dev/null 2>&1;
# eval "$($base_dir/mamba-root/bin/conda shell.bash hook)"
# mamba create -n datagen numpy scipy pathlib pandas smt --yes
# #>/dev/null 2>&1;
# # "

# Set up important variables here
declare -i sel
declare -i tasks
declare -i nfiles

# sel=`cat nodes.txt | wc -l`
# echo "sel=$sel"
sel=100
tasks=$sel*$NCPUS
echo "tasks=$tasks"

num1=1
num2=1000000
range_size=23
# Do this to prevent output data overwrite
n=$(date +%d_%b_%Y-%T)
# n="29_Feb_2024-21:00:45"
data_dir="data1"
mkdir -p $data_dir

# Activate the datagen environment with the smt library
eval "$(conda shell.bash hook)"
conda activate tf_smt
cd $base_dir
conda info --envs

# Perform LHS sampling and generate the range data file
# Arguments are:
# 1. Number of combinations
# 2. Timestamp for ANN range output file (To prevent files from being overwritten)
# 3. Name of the directory containing all the generated data
# 4. Range size (Normally 23) ! NOTE: HAS BEEN REMOVED!

time python3 range_gen_script.py $num2 $n $data_dir

time seq $num1 $num2|parallel --sshloginfile $PBS_NODEFILE -j$NCPUS "
# SSH setup
export LD_PRELOAD=/usr/lib64/libcrypto.so.1.1:$LD_PRELOAD

# Create directories specified in the sequence( num1 to num2) and copy reference 
# files from lpn folder into each directory
cd $base_dir
mkdir -p $base_dir/$data_dir/{} ; cp -r $base_dir/lpn/* $base_dir/$data_dir/{};

# Activate the mamba shell
eval "$($base_dir/mamba-root/bin/conda shell.bash hook)"
source $base_dir/mamba-root/bin/activate datagen

# Create all of the allinputs.csv files
# Arguments are:
# 1. Data directory
# 2. Folder number
# 3. Timestamp for ANN range output file
# 4. Directory from where PBS file is submitted (i.e. also location of range data file)
python3 -W "ignore" "$base_dir/create_inputs.py" $data_dir {} $n $base_dir;

# Run python script to create all the parameters.f and initvals.f files
cd $base_dir;\
python3 -W "ignore" "$base_dir/gen_input.py" $base_dir/$data_dir/{}/;

# Intel compilers and libraries setup:
source $base_dir/oneapi/setvars.sh $base_dir/oneapi/>/dev/null 2>&1

# IMPORTANT NOTE: Move to the directory where the fortran file needs to be compiled. Without this, the compiled rk4 library will NOT be generated.
cd $base_dir/$data_dir/{}

# Compile and run all instances of the LPN
python -m numpy.f2py -c -m python_rk4 $base_dir/$data_dir/{}/py_rk4_in.f90 --quiet --compiler=intelem --fcompiler=intelem --f90flags='' --opt='-fast' --include-paths $base_dir/$data_dir/{}>/dev/null 2>&1;
python3 -W "ignore" $base_dir/$data_dir/{}/rk4_run_ef.py $base_dir/$data_dir/{}/

# Now remove the unnecessary files
cd $base_dir/$data_dir/{};
shopt -s extglob;
rm -- !(summary_data.csv);
"

mkdir -p "$base_dir/output"

# Collate the output data
# Arguments are:
# 1. Number of combinations
# 2. Timestamp for ANN domain output file
# 3. Name of data directory
# time seq 1 "$tasks"|parallel --sshloginfile $PBS_NODEFILE -j$NCPUS "python3 $base_dir/collate_parallel.py $num2 {} $tasks $n $data_dir"
time seq 1 $tasks|parallel --sshloginfile $PBS_NODEFILE -j$NCPUS "module load anaconda3/2022.10-gcc/9.5.0
export LD_PRELOAD=/usr/lib64/libcrypto.so.1.1:$LD_PRELOAD
python3 $base_dir/collate_parallel.py $num2 {} $tasks $n $data_dir"

time python3 $base_dir/final_compile.py $base_dir/output $n $tasks