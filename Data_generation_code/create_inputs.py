#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon August 31 10:25:08 2020

@author: Akash Gupta akashg@clemson.edu
"""
import numpy as np
import pandas as ps
from pathlib import PurePath
import os
import csv
import sys
import glob


data_dir = sys.argv[1]
j = int(sys.argv[2])
date = sys.argv[3]


# Find the ANN_range file in the parent directory
dpath = PurePath(sys.argv[4])
flist = glob.glob(str(dpath)+"/ANN_range"+date+".csv")
# flist is actually a list of paths, not a string, so access the first element of that list
# which is the all_inputs.csv file we need
fpath = PurePath(flist[0])

# Current directory number 'j'
current_dir = dpath/PurePath(data_dir)/PurePath(str(j))

# Load range data
# x = np.genfromtxt(fpath,delimiter=',')
''' Using this instead of np.genfromtext is more memory efficient since you dont need to load the entire file. 
    You iterate through and pick up the line you need.
'''
with open(fpath) as f:
    for i, line in enumerate(f):
        if (i == j):
            x = [float(y) for y in line.split(',')]

# Get reference LPN allinputs.csv file
inputs = ps.read_csv(dpath/"all_inputs.csv")

# Modify the heart parameters
# for i in [range(1,20)]:                             # Indexes start from zero hence the slightly odd numbers
#     inputs.iloc[ (93+i) , 2] = x[j-1,i]
inputs.iloc[94,  2] = x[1]              # hr
inputs.iloc[106, 2] = x[2]              # EmaxRV
inputs.iloc[107, 2] = x[3]              # EoffsetRV
inputs.iloc[108, 2] = x[4]              # EmaxLV
inputs.iloc[109, 2] = x[5]              # EoffsetLV
# inputs.iloc[ 110 , 2 ] = x[6 ]              # Vrv0
# inputs.iloc[ 111 , 2 ] = x[7 ]              # Vlv0
# inputs.iloc[ 112 , 2 ] = x[7 ]              # Av_couple

# # Modify the remaining parameters
# inputs.iloc[   0 , 2 ] = x[0 ]              # scale_init
# inputs.iloc[ 120 , 2 ] = x[9 ]              # Lao
# inputs.iloc[ 172 , 2 ] = x[9]              # Cao
# inputs.iloc[ 118 , 2 ] = x[11]              # Lpul
# inputs.iloc[ 193 , 2 ] = x[12]              # Clung1
# inputs.iloc[ 194 , 2 ] = x[12]              # Clung2

# Modify the remaining parameters
inputs.iloc[0, 2] = x[0]              # scale_init
# inputs.iloc[ 120 , 2 ] = x[9 ]              # Lao
inputs.iloc[172, 2] = x[6]              # Cao
# inputs.iloc[ 118 , 2 ] = x[11]              # Lpul
# inputs.iloc[ 193 , 2 ] = x[7]              # Clung1
# inputs.iloc[ 194 , 2 ] = x[7]              # Clung2

# # Modify the SVR (All systemic Resistances are scaled together.) NOTE: Coronary resistance is not included!
# inputs.iloc[ 166 , 2 ] = x[13]              # scale_Rcrb
# inputs.iloc[ 169 , 2 ] = x[13]              # scale_Rh
# inputs.iloc[ 171 , 2 ] = x[13]              # scale_Rsvc
# inputs.iloc[ 177 , 2 ] = x[13]              # scale_Rthao
# inputs.iloc[ 178 , 2 ] = x[13]              # scale_Rabao
# inputs.iloc[ 185 , 2 ] = x[13]              # scale_Rabivc
# inputs.iloc[ 186 , 2 ] = x[13]              # scale_Rthivc
# inputs.iloc[ 190 , 2 ] = x[13]              # scale_Rl
# inputs.iloc[ 191 , 2 ] = x[13]              # scale_Rk
# inputs.iloc[ 192 , 2 ] = x[13]              # scale_Ri

# Modify the SVR (All systemic Resistances are scaled together.) NOTE: Coronary resistance is not included!
inputs.iloc[166, 2] = x[7]              # scale_Rcrb
inputs.iloc[169, 2] = x[7]              # scale_Rh
inputs.iloc[171, 2] = x[7]              # scale_Rsvc
inputs.iloc[177, 2] = x[7]              # scale_Rthao
inputs.iloc[178, 2] = x[7]              # scale_Rabao
inputs.iloc[185, 2] = x[7]              # scale_Rabivc
inputs.iloc[186, 2] = x[7]              # scale_Rthivc
inputs.iloc[190, 2] = x[7]              # scale_Rl
inputs.iloc[191, 2] = x[7]              # scale_Rk
inputs.iloc[192, 2] = x[7]              # scale_Ri

# scale_Rleg NOTE: Included in SVR scaling ONLY FOR RESTING! SWITCH OFF FOR EXERCISE!!
inputs.iloc[182, 2] = x[7]

# Modify the PVR (assumed two pul. branches,both scaled together)
inputs.iloc[195, 2] = x[8]              # scale_Rlung1
inputs.iloc[196, 2] = x[8]              # scale_Rlung2

# # Modify Leg resistances and capacitances
# inputs.iloc[ 182 , 2 ] = x[15]              # scale_Rleg
# inputs.iloc[ 180 , 2 ] = x[16]              # scale_Clega
# inputs.iloc[ 181 , 2 ] = x[16]              # scale_Clegv

# Modify all systemic capacitances
inputs.iloc[165, 2] = x[9]              # scale_Ccrb
inputs.iloc[168, 2] = x[9]              # scale_Ch
inputs.iloc[170, 2] = x[9]              # scale_Csvc
inputs.iloc[175, 2] = x[9]              # scale_Cthao
inputs.iloc[176, 2] = x[9]              # scale_Cabao
inputs.iloc[183, 2] = x[9]              # scale_Cabivc
inputs.iloc[184, 2] = x[9]              # scale_Cthivc
inputs.iloc[187:190, 2] = x[9]              # scale_Cl, Ck, Ci
# Clung1 NOTE: Included in SVR scaling ONLY FOR RESTING! SWITCH OFF FOR EXERCISE!!
inputs.iloc[193, 2] = x[9]
# Clung2 NOTE: Included in SVR scaling ONLY FOR RESTING! SWITCH OFF FOR EXERCISE!!
inputs.iloc[194, 2] = x[9]


# Scale Raov
inputs.iloc[208, 2] = x[10]              # scale_Raov

# Write out modified csv file
inputs_file_name = "all_inputs" + str(j) + ".csv"
file_path = current_dir/inputs_file_name
inputs.to_csv(file_path, index=False)
# ----------------------------------------------------------------------------------------------------------------------------
