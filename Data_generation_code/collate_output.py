#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code is for collating the generated output data from the LPN runs
Created on Tue June 02 2020
@author: Akash Gupta akashg@clemson.edu
"""

import os
import sys
import numpy as np
from pathlib import PurePath
import pandas as pd

top_dir = PurePath(os.path.dirname(os.path.realpath(__file__)))
#fpath = top_dir/"ANN_domain.txt"
#fpath = top_dir/"data3"/str(sys.argv[1])/"ANN_domain.txt"
oname = "ANN_domain" + sys.argv[2] + ".csv"
opath = top_dir/"output"/oname
t = pd.DataFrame()

try:
    for i in range(1, int(sys.argv[1])+1):

        try:
            fpath = top_dir/sys.argv[3]/str(i)/"summary_data.csv"
            # Load the ANN_range file from the specified folder
            domain = np.genfromtxt(fname=fpath, delimiter=",")
            # # Append the row number to the front of the array so we can sort it later
            # domain_indexed = np.append(i,domain)
            # print(domain)
            # print(domain.shape)
            # Transform to a pandas dataframe
            out = pd.DataFrame(domain)
            # Transpose the dataframe so that we write out the results row-wise
            t = t.append(out)

            if (float(i) % 10000 == 0):
                print(f"Directory number {i} finished.")

        except IOError:
            print(f"Error: output for directory number {i} is missing.")
            continue
        except ValueError:
            print(
                f"Error: output in directory number {i} contains NaNs/missing values.")
            continue
    # Write out to a csv file in append mode
    t.drop(t.columns[[0]], axis=1, inplace=True)
    t.to_csv(opath, mode='a', index=False, header=False,
             line_terminator='\n', float_format='%.4f')
except IOError:
    print("Error: Output file could not be generated.")
