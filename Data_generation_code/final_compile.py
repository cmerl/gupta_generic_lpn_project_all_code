import glob
import pandas as pd
import numpy as np
import sys
import math
from pathlib import PurePath

dpath = sys.argv[1]
name  = sys.argv[2]
tasks = int(sys.argv[3])

out   = PurePath(sys.argv[1])
oname = "compiled" + name + "_" + ".csv"
opath = out/oname
t     = pd.DataFrame()

for i in range(1, tasks+1):
    try:
        searchpath   = PurePath(dpath)/PurePath("prelim" +
                                              name + "_" + str(i) + ".csv")
        searchstring = searchpath.as_posix()
        flist        = glob.glob(searchstring)
        # Load the ANN_range file from the specified folder
        try:
            domain = np.genfromtxt(flist[0], delimiter=",", dtype=np.float32)
        except IndexError:
            print(f"Error: Prelim files could not be found in {dpath}.")
            print(f"Search String used:{searchpath}")

        # Transform to a pandas dataframe
        out = pd.DataFrame(domain)
        # Transpose the dataframe so that we write out the results row-wise
        # t   = t.append(out)
        t = pd.concat([t, out], axis=0, join='outer', copy=False, ignore_index=True)

    except IOError:
        print(f"Note: Compiled results file {i} is missing. If the length of the output file is \
              correct, ignore this message.")
    continue

t.to_csv(opath, mode='a', index=False, header=False,
         lineterminator='\n', float_format='%.8f')