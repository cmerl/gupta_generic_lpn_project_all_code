#!/usr/bin/env python
# coding: utf-8

import sys
import numpy as np
import pandas as pd
from keras import layers,models
from keras import callbacks
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.preprocessing import QuantileTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
import matplotlib.pyplot as plt
import datetime
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from pathlib import PurePath

# Directory to which DNN should be saved.
dpath = PurePath(sys.argv[1])
ncpus = int(sys.argv[2])

# Set up some basic numbers like num of predictors,num of predicted, and data set size to truncate to.
data_size = int(1e6)
num_predictors = 19 + 1 # +1 because of skewness,
num_predicted = 6 + 1 # +1 because of Raov


# In[3]:



domain_path  = ["/scratch/akashg/Data_generation_code/output/compiled24_May_2024-22:11:56_.csv"]
                # "/scratch/akashg/datagen/output/compiled16_Mar_2024-22:59:51_.csv"]


dataSet  = pd.read_csv(filepath_or_buffer = domain_path[0],  header=None, skipinitialspace=True, dtype=np.float64)

# dataSet2 = pd.read_csv(filepath_or_buffer = domain_path[1], header=None, skipinitialspace=True, dtype=np.float64)

# dataSet3 = pd.read_csv(filepath_or_buffer = domain_path[2], header=None, skipinitialspace=True, dtype=np.float64)

# dataSet4 = pd.read_csv(filepath_or_buffer = domain_path[3], header=None, skipinitialspace=True, dtype=np.float64)



# # # frames = [dataSet, dataSet2, dataSet3, dataSet4, dataSet5, dataSet6, dataSet7]
# frames = [dataSet, dataSet2]
# dataSet = pd.concat(frames,
#     axis=0,
#     join="outer",
#     ignore_index=True,
#     keys=None,
#     levels=None,
#     names=None,
#     verify_integrity=True,
#     copy=False)


# del(dataSet2)

# del(dataSet3)

# del(dataSet4)

# # # del(dataSet5)

# # # del(dataSet6)


# In[11]:


range_path  = "/scratch/akashg/Data_generation_code/ANN_range24_May_2024-22:11:56.csv"
# range_path2 = "/scratch/akashg/datagen/ANN_range16_Mar_2024-22:59:51.csv"
# range_path4 = "/scratch/akashg/datagen/ANN_range20_Dec_2023-01:28:34.csv"
# range_path5 = "/scratch/akashg/Protocol_data_gen/ANN_range08_May_2023-20:17:22.csv"
# range_path6 = "/scratch/akashg/Protocol_data_gen/ANN_range09_May_2023-20:35:13.csv"
# range_path7 = "/scratch/akashg/Protocol_data_gen/ANN_range10_May_2023-11:52:07.csv"

y_raw  = pd.read_csv(filepath_or_buffer = range_path,  skipinitialspace=False, dtype=np.double)
# y_raw2 = pd.read_csv(filepath_or_buffer = range_path2, skipinitialspace=False, dtype=np.double)
# y_raw3 = pd.read_csv(filepath_or_buffer = range_path3, skipinitialspace=False, dtype=np.double)
# y_raw4 = pd.read_csv(filepath_or_buffer = range_path4, skipinitialspace=False, dtype=np.double)
# y_raw5 = pd.read_csv(filepath_or_buffer = range_path5, skipinitialspace=False, dtype=np.double)
# y_raw6 = pd.read_csv(filepath_or_buffer = range_path6, skipinitialspace=False, dtype=np.double)
# y_raw7 = pd.read_csv(filepath_or_buffer = range_path6, skipinitialspace=False, dtype=np.double)


# # # # # frames = [y_raw, y_raw2, y_raw3, y_raw4, y_raw5, y_raw6, y_raw7]
# frames = [y_raw, y_raw2]
# y_raw = pd.concat(frames,
#     axis=0,
#     join="outer",
#     ignore_index=True,
#     keys=None,
#     levels=None,
#     names=None,
#     verify_integrity=True,
#     copy=False)


# del(y_raw2)
# del(y_raw3)
# del(y_raw4)
# # # # del(y_raw5)
# # # # del(y_raw6)


# dataSet.reset_index(drop=True,inplace=True)
# y_raw.reset_index(drop=True,inplace=True)

y_trunc = pd.DataFrame()
y_trunc = y_raw.iloc[0:data_size,:]
# y_trunc.tail()
# y_trunc = y_trunc._to_pandas()


# In[ ]:


# y_trunc.hist(figsize=(10,10))


# In[20]:


del(y_raw)


# In[21]:


'''Create a combined dataframe with predictors and predicted data. The combined dataset is useful later on since dropping NaN and EF=0 columns will drop 
data from both predictors and predicted '''
index = range(data_size)

# Columns for resting (30 predictors)
columns = ['HR','Mean Aortic Pressure','Systolic Aortic Pressure','Diastolic_Pressure','Pulse Pressure','scale_SVR','Max LV Volume','Min LV Volume',\
           'Max RV volume','Min RV Volume','Mean PA Pressure','Systolic PA pressure','PA Pulse pressure','scale_PVR','LVEF','RVEF',\
           'Systemic capacitor scale','Mean_RA_Pressure','Mean_LA_Pressure','skewness']#,'tas_percent','t1_percent','Vra0','Vla0','CcsaR','csaR',\
#            'dsaR','CcsaL','csaL','dsaL','SVR_ref','PVR_ref']
x_trim = pd.DataFrame(0,index=index,columns = columns, dtype=float)


# In[22]:


x_trim.iloc[:,0] = y_trunc.iloc[:,1] # Get the HR data
# x_trim.head()


# In[23]:


# x_trim.shape


# In[24]:


# x_trim.tail()


# In[25]:


# Perform data processing on the trimmed data and store this in x_processed

x_processed = x_trim
# Set MAP and other LPN output quantities
x_processed.iloc[:,1] = np.array(dataSet.iloc[2::3,35]) # Mean Aortic Pressure
x_processed.iloc[:,2] = np.array(dataSet.iloc[0::3,35]) # Systolic Aortic Pressure
x_processed.iloc[:,3] = np.array(dataSet.iloc[1::3,35]) # Diastolic Pressure
x_processed.iloc[:,4] = np.array(dataSet.iloc[0::3,35]) - np.array(dataSet.iloc[1::3,35]) # Pulse Pressure
x_processed.iloc[:,6] = np.array(dataSet.iloc[0::3,24]) # Max LV Volume
x_processed.iloc[:,7] = np.array(dataSet.iloc[1::3,24]) # Min LV Volume
x_processed.iloc[:,8] = np.array(dataSet.iloc[0::3,20]) # Max RV Volume
x_processed.iloc[:,9] = np.array(dataSet.iloc[1::3,20]) # Min RV Volume
x_processed.iloc[:,10] = np.array(dataSet.iloc[2::3,38]) # Mean PA Pressure
x_processed.iloc[:,11] = np.array(dataSet.iloc[0::3,38]) # Systolic PA Pressure
x_processed.iloc[:,12] = np.subtract( np.array(dataSet.iloc[0::3,38]) , np.array(dataSet.iloc[1::3,38]) ) # PA Pulse Pressure

# Calculate LVEF
sv = np.subtract( np.array(x_processed.iloc[:,6]) , np.array(x_processed.iloc[:,7]) )
x_processed.iloc[:,14] = np.divide( sv , np.array(x_processed.iloc[:,6]) )

# Calculate RVEF
sv = np.subtract( np.array(x_processed.iloc[:,8]) , np.array(x_processed.iloc[:,9]) )
x_processed.iloc[:,15] = np.divide( sv , np.array(x_processed.iloc[:,8]) )

# Mean Atrial Pressures
x_processed.iloc[:,17] = np.array(dataSet.iloc[2::3,30]) # Mean RA Pressure
x_processed.iloc[:,18] = np.array(dataSet.iloc[2::3,32]) # Mean LA Pressure

# Skewness
x_processed.iloc[:,19] = np.array(dataSet.iloc[2::3,62])

x_processed.iloc[:,0:20]


# In[26]:


# x_processed.iloc[:,4] = np.subtract(dataSet.iloc[0::3,35],dataSet.iloc[1::3,35]) # Pulse Pressure

# x_processed.iloc[:,12] = np.subtract( dataSet.iloc[0::3,38] , dataSet.iloc[1::3,38])  # PA Pulse Pressure

# # Calculate LVEF
# sv = np.subtract(x_processed.iloc[:,6] , x_processed.iloc[:,7])
# x_processed.iloc[:,14] = np.divide( sv , x_processed.iloc[:,6])

# # Calculate RVEF
# sv = np.subtract(x_processed.iloc[:,8] , x_processed.iloc[:,9])
# x_processed.iloc[:,15] = np.divide( sv , x_processed.iloc[:,8])


# In[27]:


# x_processed.shape


# In[28]:


# Assign all of the other values
x_processed.iloc[:,13] = y_trunc.iloc[:,8] #scale_PVR
x_processed.iloc[:,5] = y_trunc.iloc[:,7] #scale_SVR
x_processed.iloc[:,16] = y_trunc.iloc[:,9] #scale_SystemicC
# x_processed.iloc[:,16] = y_trunc.iloc[:,15]
# x_processed.iloc[:,17] = y_trunc.iloc[:,14]
x_processed.iloc[:,7:20]
#x_processed.tail()


# In[29]:


# x_processed.hist(figsize=(20,20))


# In[30]:


# x_processed.shape


# In[31]:


# x_processed.head()


# In[32]:


# Add the LPN input columns here to the x processed dataset. Multiple quantities removed since they are not predicted well.
x_processed.insert(num_predictors, "scale_EmaxRV" ,y_trunc.iloc[:,2],allow_duplicates=False)
x_processed.insert(num_predictors+1, "scale_EoffsetRV" ,y_trunc.iloc[:,3],allow_duplicates=False)
x_processed.insert(num_predictors+2, "scale_EmaxLV" ,y_trunc.iloc[:,4],allow_duplicates=False)
x_processed.insert(num_predictors+3, "scale_EoffsetLV" ,y_trunc.iloc[:,5],allow_duplicates=False)
# x_processed.insert(32, "scale_Vrv0" ,y_trunc.iloc[:,5],allow_duplicates=False)
# x_processed.insert(33, "scale_Vlv0" ,y_trunc.iloc[:,6],allow_duplicates=False)
# x_processed.insert(34, "scale_AVCouple" ,y_trunc.iloc[:,7],allow_duplicates=False)
# x_processed.insert(34, "scale_Lao" ,y_trunc.iloc[:,8],allow_duplicates=False)
x_processed.insert(num_predictors+4, "scale_Cao" ,y_trunc.iloc[:,6],allow_duplicates=False)
# x_processed.insert(36, "Scale_Lpul" ,y_trunc.iloc[:,10],allow_duplicates=False)
# x_processed.insert(num_predictors+5, "scale_Clung" ,y_trunc.iloc[:,7],allow_duplicates=False)
x_processed.insert(num_predictors+5, "scale_init" ,y_trunc.iloc[:,0],allow_duplicates=False)
x_processed.insert(num_predictors+6, "scale_Raov" ,y_trunc.iloc[:,10],allow_duplicates=False)




# Check for the total number of NaN rows
x_processed.isna().sum()


# In[40]:


# Drop NaN rows
x_processed=x_processed.dropna()



# Filter the dataset based on realistic values of LVEF and RVEF
x_data = x_processed[x_processed['LVEF'].between(0.05, 0.95)]
x_data = x_data[x_data['RVEF'].between(0.05, 0.95)]
x_data.shape


x_data = x_data[x_data['Mean Aortic Pressure'].between(50, 120)]
x_data = x_data[x_data['Systolic Aortic Pressure'].between(80, 160)]
x_data = x_data[x_data['Max LV Volume'].between(0, 250)]
x_data = x_data[x_data['Min LV Volume'].between(0, 250)]
x_data = x_data[x_data['Max RV volume'].between(0, 250)]
x_data = x_data[x_data['Min RV Volume'].between(0, 250)]
x_data = x_data[x_data['Mean_RA_Pressure'].between(0, 10)]
# x_data = x_data[x_data['Mean_LA_Pressure'].between(0.0, 40)]
x_data = x_data[x_data['Mean_LA_Pressure'].between(0.0, 12)]
# x_data = x_data[x_data['Mean PA Pressure'].between(0.0, 19)]
# x_data = x_data[x_data['Systolic PA pressure'].between(0, 60)]
x_data = x_data[x_data['Systolic PA pressure'].between(5, 40)]
x_data = x_data[x_data['Diastolic_Pressure'].between(50, 110)]
x_data = x_data[x_data['Pulse Pressure'].between(25, 60)]
x_data = x_data[x_data['Mean PA Pressure'].between(8, 19)]

# Assign the predictor (domain) data to x_df
x_df = x_data.iloc[:,0:num_predictors] # 0:num_predictors (and not 0:num_predictors-1) works because the HR column is added
# x.tail()
# x_df.iloc[:,11:]



# Assign the predicted (range) data to y_df
y_df = x_data.iloc[:,num_predictors:]

# Drop some of the unnecessary columns
x_df.drop(labels=['Max LV Volume','Min LV Volume','Pulse Pressure','LVEF','RVEF','PA Pulse pressure'],axis=1,inplace=True)

# Convert to np array for training purposes
y = np.array(y_df)
x = np.array(x_df)


# Scale the data
# Scale the data

# scaler_x = StandardScaler()
# scaler_y = StandardScaler()
scaler_x = MinMaxScaler()
scaler_y = MinMaxScaler()
# scaler_x = PowerTransformer(method='box-cox', standardize=True, copy=True)
# scaler_y = PowerTransformer(method='box-cox', standardize=True, copy=True)
# scaler_x = RobustScaler()
# scaler_y = RobustScaler()
# scaler_x = QuantileTransformer()
# scaler_y = QuantileTransformer()


# In[74]:


# # DEBUG ONLY!
# xscale = x
# yscale = y


# In[75]:


# NOTE: Run this cell at least once when using new data!
from joblib import dump, load
scaler_x.fit(x)
xscale = scaler_x.transform(x)
scaler_y.fit(y)
yscale = scaler_y.transform(y)


# In[77]:


# xscale.max(axis=0)


# In[78]:


# xscale.min(axis=0)


# In[79]:


# # If scaler data has been saved previously, load it here.
# import joblib
# scaler_x = joblib.load('scaler_x_resting_2mil.gz')
# scaler_y = joblib.load('scaler_y_resting_2mil.gz')
# xscale = scaler_x.transform(x)
# yscale = scaler_y.transform(y)


# In[80]:


# Split the available data into training and test sets. test_size sets the fraction of data to be held back for testing
X_train, X_test, y_train, y_test = train_test_split(xscale, yscale,test_size=0.1,random_state=20240318)


# In[81]:


print(f'Shape of training set X:{X_train.shape!r}')


# In[82]:


print(f'Shape of training set Y:{y_train.shape!r}')
# y_train.shape


# In[83]:


print(f'Shape of testing set X:{X_test.shape!r}')


# In[84]:


print(f'Shape of testing set Y:{y_test.shape!r}')

"""
ResNet model for regression of Keras.
Optimal model for the paper
"Chen, D.; Hu, F.; Nian, G.; Yang, T. Deep Residual Learning for Nonlinear Regression. Entropy 2020, 22, 193."
Depth:28
Width:16
"""
def identity_block(input_tensor,units):
	"""The identity block is the block that has no conv layer at shortcut.
	# Arguments
		input_tensor: input tensor
		units:output shape
	# Returns
		Output tensor for the block.
	"""
	x = layers.Dense(units)(input_tensor)
	x = layers.BatchNormalization()(x)
	x = layers.Activation('relu')(x)

	x = layers.Dense(units)(x)
	x = layers.BatchNormalization()(x)
	x = layers.Activation('relu')(x)

	x = layers.Dense(units)(x)
	x = layers.BatchNormalization()(x)

	x = layers.add([x, input_tensor])
	x = layers.Activation('relu')(x)

	return x

def dens_block(input_tensor,units):
	"""A block that has a dense layer at shortcut.
	# Arguments
		input_tensor: input tensor
		unit: output tensor shape
	# Returns
		Output tensor for the block.
	"""
	x = layers.Dense(units)(input_tensor)
	x = layers.BatchNormalization()(x)
	x = layers.Activation('relu')(x)

	x = layers.Dense(units)(x)
	x = layers.BatchNormalization()(x)
	x = layers.Activation('relu')(x)

	x = layers.Dense(units)(x)
	x = layers.BatchNormalization()(x)

	shortcut = layers.Dense(units)(input_tensor)
	shortcut = layers.BatchNormalization()(shortcut)

	x = layers.add([x, shortcut])
	x = layers.Activation('relu')(x)
	return x


def ResNet50Regression():
	"""Instantiates the ResNet50 architecture.
	# Arguments        
		input_tensor: optional Keras tensor (i.e. output of `layers.Input()`)
			to use as input for the model.        
	# Returns
		A Keras model instance.
	"""
	Res_input = layers.Input(shape= (len(x_df.columns) ,))

	width = 16

	x = dens_block(Res_input,width)
	x = identity_block(x,width)
	x = identity_block(x,width)

	x = dens_block(x,width)
	x = identity_block(x,width)
	x = identity_block(x,width)
	
	x = dens_block(x,width)
	x = identity_block(x,width)
	x = identity_block(x,width)
    
	x = layers.BatchNormalization()(x)
	x = layers.Dense(len(y_df.columns), activation='linear')(x)
	model = models.Model(inputs=Res_input, outputs=x)

	return model


# Intel specific optimization commands/flags
import os
from tensorflow.config.threading import set_intra_op_parallelism_threads
from tensorflow.config import set_soft_device_placement
from tensorflow.config.optimizer import set_experimental_options

os.system("export TF_ENABLE_ONEDNN_OPTS=1")
os.system("export KMP_AFFINITY=granularity=fine,compact,1,0")
os.system("export KMP_BLOCKTIME=0")
set_soft_device_placement(True)
set_intra_op_parallelism_threads(ncpus)
set_experimental_options({'auto_mixed_precision_onednn_bfloat16':True})


# In[92]:


##############################Build Model################################
# model = ResNet50Regression()

# model.compile(loss='mse', optimizer='adam', metrics=['mae'])
# # model.summary()

# history = model.fit(X_train, y_train, epochs=5, batch_size=1024, verbose=2, 
#                     callbacks=[callbacks.EarlyStopping(monitor='val_loss', patience=200,verbose=2,restore_best_weights=True, mode='auto'),], 
#                     validation_split=0.1)


##############################Build Model################################
model = ResNet50Regression()

# weights = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
# weights = [0.01, 0.05, 0.01, 1.0, 0.01, 0.01, 0.01]

model.compile(loss='mse', optimizer='adam', metrics=['mae'])
model.summary()

#compute running time
starttime = datetime.datetime.now()


history = model.fit(X_train, y_train, epochs=3000, batch_size=2048, verbose=2, 
                    callbacks=[callbacks.EarlyStopping(monitor='val_loss', patience=100, verbose=2,\
                    restore_best_weights=True, mode='auto')], validation_split=0.05)

endtime = datetime.datetime.now()

from pathlib import Path
Path("dpath").mkdir(parents=True, exist_ok=True)

model.save(str(dpath) + '/DNN.keras')

# Save the scaler data
dump(scaler_x, str(dpath) + '/scaler_x.gz')
dump(scaler_y, str(dpath) + '/scaler_y.gz')


# In[ ]:


endtime = datetime.datetime.now()


# In[ ]:


time_elapsed = endtime - starttime
time_elapsed


# In[ ]:


print(f'Elapsed training time: {time_elapsed!r}')


# In[ ]:


print(f'Shape of independant validation set Y: {y_test.shape!r}')


# In[ ]:


yhat = model.predict(X_test)
yhat.shape


# In[ ]:


y_test.shape


# In[ ]:


#############################Model Predicting#################################
yhat = model.predict(X_test)

print('The test loss: ')
print(mean_squared_error(yhat,y_test))

#invert normalize
yhat_check = scaler_y.inverse_transform(yhat) 
y_test_check = scaler_y.inverse_transform(y_test) 
# y_cols = y.columns
y_cols = y_df.columns


# In[ ]:


fig, axes = plt.subplots(7, 1, figsize=(10, 50))

for r in range(len(y_cols)):
    #b = plt.figurae(figsize=(10,10))
    #a = plt.axes(aspect='equal')
    
    thisaxis = axes.flat[r]
    
    plot_x_data = y_test_check[:,r]
    plot_y_data = yhat_check[:,r]
    _=thisaxis.scatter(plot_x_data, plot_y_data)
    
    #minval = min([min(plot_x_data), min(plot_y_data)])
    #maxval = max([max(plot_x_data), max(plot_x_data)])
    
    minval = min(plot_x_data)
    maxval = max(plot_x_data)
    
    thisaxis.set_xlim([minval, maxval])
    thisaxis.set_ylim([minval, maxval])
    thisaxis.set_xlabel(f'({r}) {y_cols[r]}', fontsize=22)
    thisaxis.set_ylabel(f'Prediction', fontsize=22)
    thisaxis.set_aspect('equal')
# plt.ax.set_xticklabels(x_ticks, rotation=0, fontsize=14)
# plt.ax.set_yticklabels(y_ticks, rotation=0, fontsize=14)
#    thisaxis.tick_params(labelsize=16)
# plt.plot([0, 0], [2, 2], color='k', linestyle='-', linewidth=2)
# plt.show()
    thisaxis.annotate("",
              xy=(minval, minval), xycoords='data',
              xytext=(maxval, maxval), textcoords='data',
              arrowprops=dict(arrowstyle="-",
                              connectionstyle="arc3,rad=0.",
                              linewidth=3,
                              alpha  = 0.8,), 
              )

plt.savefig(fname = dpath/PurePath('1mil_retune.png'), bbox_inches = 'tight') 