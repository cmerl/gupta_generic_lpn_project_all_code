#!/bin/bash

#SBATCH --job-name test
#SBATCH --nodes 1
#SBATCH --tasks-per-node 1
#SBATCH --cpus-per-task 16
#SBATCH --gpus-per-node v100:1
#SBATCH --mem 62gb
#SBATCH --time 04:00:00
#SBATCH --constraint interconnect_hdr

jobperf -record -w -http -rate 10s &
export OMP_NUM_THREADS=16
module purge
module load gcc/13.2.0 anaconda3/2023.09-0 cuda/12.3.0 spack
spack load cudnn

cd $SLURM_SUBMIT_DIR
saved_model_dir="1mil_retune_16w_minmax_filter_2"

mkdir -p $SLURM_SUBMIT_DIR/$saved_model_dir

# Intel compilers and libraries setup:
cp /home/akashg/intel/oneapi.tgz $TMPDIR;
cd $TMPDIR;
tar xzf oneapi.tgz
source $TMPDIR/oneapi/setvars.sh $TMPDIR/oneapi/>/dev/null 2>&1;

# Install numactl
# git clone https://github.com/numactl/numactl.git
# cd numactl/
# ./autogen.sh>/dev/null 2>&1
# ./configure>/dev/null 2>&1
# make install DESTDIR=$TMPDIR>/dev/null 2>&1
# which numactl

mkdir -p $TMPDIR/DNN_training
cp /scratch/akashg/DNN_training/1mil.py $TMPDIR/DNN_training


source /scratch/akashg/mamba-root/bin/activate datagen
conda info --envs

# Intel specific options.
export KMP_AFFINITY=granularity=fine,compact,1,0
export KMP_BLOCKTIME=0
export TF_ENABLE_ONEDNN_OPTS=1
export TF_ENABLE_MKL_NATIVE_FORMAT=1
export TF_CPP_MIN_LOG_LEVEL=2 

# time numactl --cpunodebind=0 --membind=0 python3 -W "ignore" $TMPDIR/DNN_training/1mil.py $SLURM_SUBMIT_DIR/$saved_model_dir/ $SLURM_CPUS_PER_TASK>$SLURM_SUBMIT_DIR/$saved_model_dir/training.log 2>&1;
time python3 -W "ignore" $TMPDIR/DNN_training/1mil.py $SLURM_SUBMIT_DIR/$saved_model_dir/ $SLURM_CPUS_PER_TASK>$SLURM_SUBMIT_DIR/$saved_model_dir/training.log 2>&1;
