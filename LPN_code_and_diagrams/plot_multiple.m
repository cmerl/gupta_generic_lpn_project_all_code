clear variables;
close all;

results = load('AllData');
% results1 = load('AllData_palmetto');
% out = results - results1;
% check = any(out);

hr = 80;
% hr = 60;
ncycles  = 3;
lcycle = ceil ( (60/hr)*1000 );
[dim1 ,dim2]= size(results);
start = dim1 - (ncycles*lcycle);
results_trim = results(start:end,:);

sys = 26;
cor = 3;
extra = 24;
pul = sys + cor + extra + 1;
% pul = sys + cor + 6;
pra = (sys + cor + 2);
prv = (sys + cor + 3);
pla = (sys + cor + 4);
plv = (sys + cor + 5);
ppul = (sys + cor + 10);
qsvc = (sys + cor + 12);
qthivc = (sys + cor + 17);
pao = 36;
qmit = 24;
% qav = 26;
vrv = 21;
vlv = 25;
qpa = 22;
qtri = 20;
qao=26;
qcorv = sys + 3;
% qpul = pul+4;
% qpul = pul;

Qao = results_trim(:,qao)./16.667;
Qmit = results_trim(:,qmit)./16.667;
Qpa = results_trim(:,qpa)./16.667;
Qcorv = results_trim(:,qcorv)./16.667;
Qtri = results_trim(:,qtri)./16.667;
% QaoS = Qao(1:500);
% QaoS = QaoS(QaoS~=0);
% QaoSkew = skewness(QaoS,0);
QaoSkew = skewness(Qao); %Skewness of Aortic Flow
QpulSkew = skewness(Qpa); %Skewness of Pul. Artery floe


COlv = ( max( results_trim (end - lcycle:end , 25) ) - min( results_trim (end - lcycle:end , 25) ) ) *(hr/1000);
CO_v = ( mean( results_trim (end - lcycle:end , 41) ) + mean( results_trim (end - lcycle:end , 46) ) + mean( results_trim (end - lcycle:end , 29) )) *(60/1000);
CO =  mean( results_trim (end - lcycle:end , 26)*(60/1000) ) ;
CO_r =  ( max( results_trim (end - lcycle:end , 21) ) - min( results_trim (end - lcycle:end , 21) ) ) *(hr/1000);

pla_m =  mean(results_trim(end-lcycle:end,33));
map_m = mean(results_trim(end-lcycle:end,4));
pbar_m = mean(results_trim(end-lcycle:end,39));
pivc_m = mean(results_trim(end-lcycle:end,13));
pra_m =  mean(results_trim(end-lcycle:end,31));

% Plot figures here
f1 = figure;
f1.Position = [10 10 600*2 400*2];
extraInputs = {'interpreter','tex','fontsize',14};
%% LV Plots
corr_time = ( results_trim(:,30) );%-(40-(lcycle*ncycles)/1000)); %.*1000;
% Create first axis and change properties
ax1 = subplot(2,3,1);
% Create Line plot and change properties
p1 = plot(results_trim(:,vlv), results_trim(:,plv) );
p1.LineWidth = 2.0;

% max_x = ( max(results_trim(:,vlv)) )*1.1;
% min_x = ( min(results_trim(:,vlv)) )*0.9;
% max_y = ( max(results_trim(:,plv)) )*1.1;
% min_y = ( min(results_trim(:,plv)) )*0.9;

% xlim(ax1 , [round(min_x,-1)-10 round(max_x,-1)] );
% ylim(ax1 , [round(min_y,-1) round(max_y,-1)] );

set(ax1, 'fontsize', 10.5);
xlabel(ax1, 'LV Volume (ml)', extraInputs{:} );
ylabel(ax1, 'LV Pressure (mmHg)', extraInputs{:} );
% set(findobj(ax1,'Type','text'),'FontSize', 16);


ax2 = subplot(2,3,2);
p2 = plot(corr_time, results_trim(:,pao) , corr_time , results_trim(:,plv), corr_time, results_trim(:,26)/4) ;
p2 = plot(corr_time, results_trim(:,pao) , corr_time , results_trim(:,plv)) ;
p2(1).LineWidth = 2.0;
p2(2).LineWidth = 2.0;
% p2(3).LineWidth = 2.0;
% p2(2).Color = '#2e1e3b';
legend('Pao','Plv','Location','best');
% ylim(ax2 , [round(min_y,-1) round(max_y,-1)] );
ylabel(ax2, 'Pressure (mmHg)', extraInputs{:});
xlabel(ax2, 'Time (s)', extraInputs{:});

ax3 = subplot(2,3,3);
p3= plot(corr_time,Qao,corr_time,Qmit) ;
p3(1).LineWidth = 2.0;
p3(1).LineStyle = '-.';
p3(2).LineWidth = 2.0;
% p2(2).Color = '#2e1e3b';
legend('Qao','Qmit','Location','best');
ylabel(ax3, 'Flow (L/min)', extraInputs{:});
xlabel(ax3, 'Time (s)', extraInputs{:});
%% RV Plots

% Create first axis and change properties
ax4 = subplot(2,3,4);
p4 = plot(results_trim(:,vrv), results_trim(:,prv) );
p4.LineWidth = 2.0;

max_x = ( max(results_trim(:,vrv)) )*1.1;
min_x = ( min(results_trim(:,vrv)) )*0.9;
max_y = ( max(results_trim(:,prv)) )*1.1;
min_y = ( min(results_trim(:,prv)) )*0.9;

xlim(ax4 , [round(min_x,-1)-10 round(max_x,-1)] );
% ylim(ax4 , [round(min_y,-1) round(max_y,-1)] );

xlabel(ax4, 'RV Volume (ml)', extraInputs{:});
ylabel(ax4, 'RV Pressure (mmHg)', extraInputs{:});
% p1.Color ='#2E1E3B' ;
% legend('Pao','Plv');

ax5= subplot(2,3,5);
p5= plot(corr_time,results_trim(:,ppul),corr_time,results_trim(:,prv)) ;
p5(1).LineWidth = 2.0;
p5(2).LineWidth = 2.0;
% p2(2).Color = '#2e1e3b';
% ylim(ax5 , [round(min_y,-1) round(max_y,-1)] );
legend('Ppul','Prv','Location','best');
ylabel(ax5, 'Pressure (mmHg)', extraInputs{:});
xlabel(ax5, 'Time (s)', extraInputs{:});

% Qsvc = results_trim(:,qsvc)./16.667;
% Qivc = results_trim(:,qthivc)./16.667;

ax6 = subplot(2,3,6);
p6= plot(corr_time,Qpa,corr_time,Qtri) ;
p6(1).LineWidth = 2.0;
p6(1).LineStyle = '-.';
p6(2).LineWidth = 2.0;
% p2(2).Color = '#2e1e3b';
legend('Qpa','Qtri','Location','best');
ylabel(ax6, 'Flow (L/min)', extraInputs{:});

xlabel(ax6, 'Time (s)', extraInputs{:});

SVR=(map_m-pra_m)/(CO_r*(1000/60));
PVR=(pbar_m-pla_m)/(CO_r*(1000/60));

get(ax1,'Position');
set(ax1,'Position',[0.05 0.575 0.25 0.375]);

get(ax2,'Position');
set(ax2,'Position',[0.375 0.575 0.25 0.375]);

get(ax3,'Position');
set(ax3,'Position',[0.7 0.575 0.25 0.375]);

get(ax4,'Position');
set(ax4,'Position',[0.05 0.1 0.25 0.375]);

get(ax5,'Position');
set(ax5,'Position',[0.375 0.1 0.25 0.375]);

get(ax6,'Position');
set(ax6,'Position',[0.7 0.1 0.25 0.375]);