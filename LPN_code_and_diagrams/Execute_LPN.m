% This is the main file to run
tic
clear;
clear variables;
close all;
% clc;

%     system('gfortran -g rk4_in_Raov_Rpav.f90');
% system ('call "C:\Program Files (x86)\Intel\oneAPI\setvars.bat" && ifort /o "a.exe" rk4_lpn.f90');

Write_Input_Parameters
% str1 ='call "C:\Program Files (x86)\Intel\oneAPI\setvars.bat" && ifort /o "a.exe" Bi-Ven_rk4_1.f90';
% str1 ='call "C:\Program Files (x86)\Intel\oneAPI\setvars.bat" && ifort /o "a.exe" dec22_keo_copy.f90';
str1 ='call "C:\Program Files (x86)\Intel\oneAPI\setvars.bat" && ifort /o "a.exe" jan5_keo.f90';

% Optimization Flags
% str2 = ' /O3 /QxHOST /Qimf-precision:high';
% str2 = ' /O3';
% str2 = '';
str2 = ' /fast';


s = strcat(str1,str2);
system (s);    
fprintf('Compilation Complete \n\n');

tic
system("a.exe");		% for Windows based systems
%	system('./a.out');		% for Linux based systems
toc
fprintf('Results are Ready\n\n');

plot_multiple

%     output_write(common_FilePath,iter,balance,HR);
%     
% 	fprintf('Iteration %d Done\n\n',iter);
%     toc
%     
% end
% 
% fprintf('%s Done\n%s\n\n',sim_name,purpose);
% fclose(balance);
% 
% parameter{1} = 'EmaxLV';	% Name of parameter which is changed for the Sensitivity
% parameter{2} = '76';		% Row number of where the parameter base value is in the input.csv file
% parameter{3} = '109';		% Row number of where the parameter scale value is in the input.csv file
% 
% Parameter_Sensitivity(common_FilePath,parameter,iter);
% 
% close all;
toc